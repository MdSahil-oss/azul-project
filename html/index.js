const table = document.getElementById("table");
let currencyType = "CZK";
const partialUrl = "https://mdsahiloss-azul-project.s3.amazonaws.com/datasets/";
const datasets = [
  {
    time: "11-mar-2024 18:00 UTC",
    filename: `dataset-01.json`,
  },
  {
    time: "11-mar-2024 20:00 UTC",
    filename: `dataset-02.json`,
  },
  {
    time: "11-mar-2024 22:00 UTC",
    filename: `dataset-03.json`,
  },
  {
    time: "12-mar-2024 08:00 UTC",
    filename: `dataset-04.json`,
  },
  {
    time: "12-mar-2024 10:00 UTC",
    filename: `dataset-05.json`,
  },
  {
    time: "15-mar-2024 18:00 UTC",
    filename: `dataset-06.json`,
  },
  {
    time: "15-mar-2024 20:00 UTC",
    filename: `dataset-07.json`,
  },
  {
    time: "15-mar-2024 22:00 UTC",
    filename: `dataset-08.json`,
  },
  {
    time: "16-mar-2024 08:00 UTC",
    filename: `dataset-09.json`,
  },
  {
    time: "16-mar-2024 10:00 UTC",
    filename: `dataset-10.json`,
  },
];

function getData() {
  table.innerHTML =
    `<tr>` +
    `<th>Date & Time</th>` +
    `<th>Currency</th>` +
    `<th>Exchange rates in USD</th>` +
    `</tr>`;

  datasets.forEach((element) => {
    let url = partialUrl + element.filename;
    fetch(url)
      .then((response) => {
        // Check if the request was successful
        if (!response.ok) {
          throw new Error(
            "Network response was not ok for " +
              filename +
              " status: " +
              response.status
          );
        }
        // Parse the response as JSON
        return response.json();
      })
      .then((data) => {
        // Handle the JSON data
        table.innerHTML +=
          `<tr>` +
          `<td>${element.time}</td>` +
          `<td>${currencyType}</td>` +
          `<td>${data["data"]["rates"][currencyType]}</td>` +
          `</tr>`;
      })
      .catch((error) => {
        // Handle any errors that occurred during the fetch
        console.error("Fetch error:", error);
      });
  });
}

document.getElementById("form").addEventListener("submit", function (event) {
  event.preventDefault();
  const temp = document.getElementById("currency-type").value;
  console.log("currency type:", temp);
  if (temp !== null && temp.trim().length > 0) {
    currencyType = temp;
  } else {
    currencyType = "CZK";
  }
  getData();
});

getData();
