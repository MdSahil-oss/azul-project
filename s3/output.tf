output "bucker_name" {
  description = "Name of the created bucket"
  value       = var.bucker_name
}

output "bucker_region" {
  description = "Region where bucket got created"
  value       = local.region
}
