resource "aws_s3_bucket" "azulProject" {
  bucket = var.bucker_name
  tags = {
    Name = var.bucker_name
  }
}

resource "aws_s3_bucket_public_access_block" "azulProject" {
  bucket = aws_s3_bucket.azulProject.id

  block_public_acls = var.block_public_access
}

resource "aws_s3_bucket_ownership_controls" "azulProject" {
  bucket = aws_s3_bucket.azulProject.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "azulProject" {
  depends_on = [aws_s3_bucket_ownership_controls.azulProject]
  bucket     = aws_s3_bucket.azulProject.id
  acl        = "private"
}

resource "aws_s3_bucket_cors_configuration" "azulProject" {
  bucket     = aws_s3_bucket.azulProject.id
  depends_on = [aws_s3_bucket.azulProject]
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
  }
}
