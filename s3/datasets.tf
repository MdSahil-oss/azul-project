resource "aws_s3_object" "pathDatasets" {
  depends_on = [
    aws_s3_bucket.azulProject
  ]
  bucket = aws_s3_bucket.azulProject.id
  acl    = "private"
  key    = "datasets/"
  source = "/dev/null"
}

resource "aws_s3_object" "pathDatasetsDataset-01" {
  depends_on = [
    aws_s3_bucket.azulProject,
    aws_s3_object.pathDatasets
  ]
  for_each = fileset("../datasets", "*")
  bucket   = aws_s3_bucket.azulProject.id
  key      = "datasets/${each.value}"
  source   = "../datasets/${each.value}"
  acl      = "public-read"
}
