variable "bucker_name" {
  type    = string
  default = "mdsahiloss-azul-project"
}

variable "block_public_access" {
  type    = bool
  default = false
}

locals {
  region = "us-east-1"
}
