
data "aws_vpc" "vpc" {
  id = var.vpc_id
}

# data "aws_subnets" "vpc_subnets" {
#   filter {
#     name   = "vpc-id"
#     values = [var.vpc_id]
#   }
# }

data "aws_subnet" "usEast1a" {
  vpc_id            = data.aws_vpc.vpc.id
  availability_zone = local.zoneA
}

data "aws_subnet" "usEast1b" {
  vpc_id            = data.aws_vpc.vpc.id
  availability_zone = local.zoneB
}
