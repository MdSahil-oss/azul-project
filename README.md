# azul-project

![cicd image](static-resources/cicd.png)

## Getting started (How to start this application?)

To run this project you must have aws profile configured with `AWS_ACCESS_KEY_ID` & `AWS_SECRET_ACCESS_KEY` credentials at least on your machine with followings tools:

- Terraform CLI
- aws CLI

To run this project start S3 bucket by following commands:

```bash
$ cd s3
$ terraform init
$ terraform apply
```

Then start EKS cluster on AWS by following commands:

```bash
$ cd eks
$ terraform init
$ terraform apply
```

Connect cluster with your system:

```bash
$ aws eks update-kubeconfig --name {CLUSTER_NAME} --region {CLUSTER_REGION}
```

Now start application inside the cluster using following command:

```bash
$ kubectl apply -f deployment.yaml
```

**Every time on making changes with code in `html/` directory change (upgrade) `IMAGE_VERSION` in `.gitlab-ci.yml` so that new version of image is built & pushed to dockerhub and started in cluster by pipeline otherwise pipeline will build & push image with the same previous version which will result in overwriting of an existing image on dockerhub which is not a good practice (Rollback to previous state of application become impossible without reverting code).**

_Pipeline update application image in cluster with rolloing update strategy so that zero downtime and application is always up & running_

## Working (How does this project work?)

On applying `deployment.yaml` via `kubectl` an application docker container runs inside Kubernetes `default` namespace within a pod which is handled by an upstream resource `deployment` and Service (of type `LoadBalancer`) makes the application accessible via an external URL for development/testing purposes (for production `ingress` is used).

On Accessing the application using the external URL following things happens:

- An HTML page is rendered to the user with a blank simple table.
- API calls are made to S3 bucket objects to fetch json data.
- Having fetched json data then table rows are filled into html table.

![table gif](static-resources/table.gif)

## Approach (What tools am I using to build this project?)

In this project I'm using gitlab CI to automate the deployment/rolling-update of the application into Kubernetes(EKS) cluster on everytime source code updation and also using other following **infrastructure & tools**:

- Terraform (as IaaC tool)
- AWS S3 (Simple Storage Service)
- Docker (Containerization tool)
- AWS EKS (Elastic Kubernetes Service)
- Bash

**Terraform:** Using terraform to automate the creation of following infrastructure components.

- S3 bucket (Also Uploading all the json files present in [datasets/](https://gitlab.com/MdSahil-oss/azul-project/-/tree/main/datasets) directory to the bucket as objects and making them publicly accessible using terraform).
- EKS cluster (Also installing gitlab agent automatically on the cluster using terraform so that connection with gitlab become feasible).

**AWS S3:** Using s3 bucket to store all the dataset present in [datasets/](https://gitlab.com/MdSahil-oss/azul-project/-/tree/main/datasets) directory as objects. These objects are json files which are accessed by the application to fetch data for table.

**Docker:** Gitlab pipeline uses docker to build container image of the application and pushes to [my repository](https://hub.docker.com/repository/docker/mdsahiloss/azul-project/general) on dockerhub that will be deployed to kuberfnetes on deployment stage.

**AWS EKS:** Pipeline deploys/roll-update a new version of containerized image of the application into EKS cluster on everytime pipeline triggers & deployment stage succeed which is accessible outside of cluster with the help of an External Service URL.

**Bash:** This is the back-bone of gitlab CI/CD, using bash in all the stages of pipeline.

Overall I can say terraform prepare the infrastructure and gitlab CI/CD start/update the application on the prepared infrastructure which is accessible via an external URL.

## How does this pipeline achieves version roll back?

Whenever the merge request is raised against the `main` branch a test stage of the pipeline runs to check whether the changes contains upgraded image version or not.
If it does then test succeeds otherwise fails.

Succeeding of the test indicates that the image version has been upgraded and there will be no image overwriting on the image registry on merging that PR and a new image will be with a diffrent version that does not exist on the image registry.

Failure indicates that the image with version already exist on the image registry, So building & pushing image with the same version will lead to image overwriting on image registry, That's why image can not be built.

Repository maintainer won't merge the PR until the test passes.

So this way, On Every change in the main branch will include a diffrent version of application image and the latest version of the main branch will be running inside Kubernetes by Pipeline automatically.

If the application admin feels dicontented with the latest version of running application then they can roll back to the previous version of application without reverting changes in code as the previous version of application image already exist on image registry (e.g dockerhub).

## How does this pipeline zero-down time?

Well! Kubernetes is a great tool, The pipeline achieves zero down time with the help of rolling update feature of kubernetes. So application is never down because pipeline never delete deployment of the application image into Kubernetes instead it roll update (means, update all the replicas of application inside cluster one by one).

For more please check rolling update feature of kubernetes.

## Proof of project runnning

![running-azul-project](static-resources/azul-project.mp4)

Sorry in the this video I'm using minikube cluster for demonstration because I deleted EKS cluster from my AWS environment before making this video and starting a simple EKS cluster on AWS takes around 20-25 minutes so decided to make video using minikube, I hope you don't mind it.

## problems I encountered during the implementation

To be very honest, I faced no problems because google is great, Wherever I stuck during implementation I looked into documentation of the technology, Like I'm using Gitlab pipeline for the first time in this project Never used before (Usually work with Github VCS). That was a bit challeging but gitlab has a great documentation support that cleared my all questions/doubts.

So yeah, That's how I built this project "learning while doing".
